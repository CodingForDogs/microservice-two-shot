# Wardrobify

Team:

- Faith - Hats microservice
- Valeria - Shoes microservice

## Design

## Shoes microservice

I have designed two models, one is the BinVO Model and the other is the Shoe model. There are two microservices which are the Shoes/api and Shoes/poll. I created both models in the Shoes/api app and used the Shoes/poll app to pull Bin data from the Wardrobe API. Each shoe exists within a Bin in the Wardrobe. The existing Wardrobe API can be accessed from the shoes polling service.

I also created two React components to show a list and details of shoes and another component to show a form to create a new shoe.

## Hats microservice

To begin, I worked on the `hats/api/poll/poller.py` file to create poller functions. Then I created models LocationVO and Hats in the hats/api/hats_rest/models.py file. After the models were completed, I opened up `hats/api/hats_rest/views.py` and began working on my encoders to work between JSON and my Python models and view functions. After I checked through my current code to make sure I didn't have any typos or errors, I completed the `hats/api/hats_rest/urls` file and then I set up insomnia so that I can start creating objects and testing if the api keys and links work.

With the JavaScript front end, I created `ghi/app/src/HatForm.js` and `ghi/app/src/HatsList.js`. In the `HatForms.js` file, my HatForm function has functions inside that pull data through the api urls and creates the HTML page using React. In my `HatsList.js` file, I used React to create the HTML table and grab data to insert into the table. I added my Hats microservice nav links into `ghi/app/src/Nav.js` and created a function that would support rendering both the Shoes and Hats microservices in `ghi/app/src/index.js`. Then I added my Hats Microservice routes to the `ghi/app/src/App.js` file.
