from django.urls import path
from .views import api_list_hats, api_detail_hat

urlpatterns = [
    path("hats/", api_list_hats, name="api_create_hats"),
    path("hats/<int:pk>/", api_detail_hat, name="show_hat_detail"),
    path(
        "locations/<int:location_vo_id>/hats/",
        api_list_hats,
        name="api_list_hats",
    ),
]
