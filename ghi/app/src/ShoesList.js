import './ShoesList.css';

function ShoesList(props) {

    const Remove= ((id)=> {
        if(window.confirm("Do you want to remove?")) {
            fetch("http://localhost:8080/api/shoes/"+id,
            {method:"DELETE"}).then(()=> {
                window.location.reload();
            }).catch((err)=>{
                console.log(err.message)
            })
        }
    })
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model Name</th>
                    <th>Manufacturer</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Closet Name</th>
                    <th>Delete Shoe</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.color }</td>
                            <td> <img src={ shoe.picture_url } alt="" width="300" /></td>
                            <td>{ shoe.bin }</td>
                            <td>
                                <button onClick={() => {Remove(shoe.id)}}>Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoesList;
