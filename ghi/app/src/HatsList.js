import './HatList.css';

function HatsList(props) {
  const Remove= ((id)=> {
    if(window.confirm("Are you sure you want to delete?")) {
      fetch("http://localhost:8090/api/hats/"+id,
      {method:"DELETE"}).then(()=> {
        window.location.reload();
      }).catch((err)=>{
        console.log(err.message)
      })
    }
  })
  return (
    <div className="backgroundhats">
      <table className="table table-striped">
        <thead id="thead">
          <tr>
            <th>Model Name</th>
            <th>Brand</th>
            <th>Color</th>
            <th>Location</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {props.hats?.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{hat.model_name}</td>
                <td>{hat.brand}</td>
                <td>{hat.color}</td>
                <td>{hat.location}</td>
                <td> <img src={hat.import_url} alt="image" width="300" /></td>
                <td>
                  <button id="deletebutton" onClick={() => {Remove(hat.id)}}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default HatsList;
