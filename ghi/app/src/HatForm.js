import React, { useEffect, useState } from "react";
import './HatList.css';

function HatForm() {
  const [locations, setLocations] = useState([])

  const [formData, setFormData] = useState({
    model_name: '',
    brand: '',
    color: '',
    location: '',
    import_url: '',
  })

  const fetchData = async () => {
    const url = "http://localhost:8100/api/locations/";

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json()
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async(event) => {
    event.preventDefault();
    Number(formData.location)
    const url = `http://localhost:8090/api/locations/${formData.location}/hats/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        model_name: "",
        brand: "",
        color: "",
        location: "",
        import_url: "",
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,

      [inputName]: value
    });
  }

  return (
    <div className="backgroundhats">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4" id="table">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">

              <div className="form-floating mb-3">
                <input
                  onChange={handleFormChange}
                  value={formData.model_name}
                  placeholder="Model Name"
                  required type="text"
                  name="model_name"
                  id="model_name"
                  className="form-control"
                />
                <label htmlFor="model_name">Model name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleFormChange}
                  value={formData.brand}
                  placeholder="Brand"
                  required type="text"
                  name="brand"
                  id="brand"
                  className="form-control" />
                <label htmlFor="brand">Brand</label>
              </div>
              <div className="form-floating mb-3">
                  <input
                    onChange={handleFormChange}
                    value={formData.color}
                    placeholder="Color"
                    required type="text"
                    name="color"
                    id="color"
                    className="form-control"
                  />
                  <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={handleFormChange}
                  value={formData.location}
                  required name="location"
                  id="location"
                  className="form-select labelcolor">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.id} value={location.id}>
                            {location.closet_name}
                        </option>
                      )
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleFormChange}
                  value={formData.import_url}
                  placeholder="Picture url"
                  required type="text"
                  name="import_url"
                  id="import_url"
                  className="form-control"
                />
                <label htmlFor="import_url">Picture url</label>
              </div>
              <button type="submit" className="btn btn-primary">
                Create
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  )}

export default HatForm;
